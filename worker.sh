#!/bin/bash
set -x

CDIR=$(dirname "$0")
mkdir -p "$CDIR/logs"
LOG=${1:-"$CDIR/logs/helios.worker.`date '+%FT%T%:z'`.log"}
echo "Starting Helios Worker `date '+%FT%T%:z'`" >"$LOG"

exec 7>&1
coproc tee -a "$LOG" >&7
exec >&"${COPROC[1]}" 2>&1

. ./env.sh

exec python manage.py celeryd -E -B --beat --concurrency=1
