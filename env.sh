#!/bin/bash
set -x
set -a # export all vars

. ./.secrets || {
  echo "Must have a .secrets file with appropriate secrets" >&2
  exit 1
}

PORT=${PORT:-8000}

#DEBUG=1
ALLOWED_HOSTS="localhost"
URL_HOST="http://${ALLOWED_HOSTS}:${PORT}"
DATABASE_URL="sqlite:///helios.production.sqlite"
SECRET_KEY="${SECRET_KEY}"

# Use local smtp server, this seems to send email fine.
EMAIL_HOST='localhost'
EMAIL_PORT='1025'
#EMAIL_HOST_USER='smtp_user_name'
#EMAIL_HOST_PASSWORD="${EMAIL_HOST_PASSWORD}"
EMAIL_USE_TLS=0

SITE_TITLE="** SITE_TITLE not set **"
WELCOME_MESSAGE="** WELCOME_MESSAGE not set **"
HELP_EMAIL_ADDRESS="help@email.address"

ALLOW_ELECTION_INFO_URL=1

# Do not show login options, since this site is mostly used by voters
SHOW_LOGIN_OPTIONS=0
SHOW_USER_INFO=0

#AUTH_ENABLED_AUTH_SYSTEMS="password,auth0"
AUTH_ENABLED_AUTH_SYSTEMS="password"

DEFAULT_FROM_EMAIL="default@from.email"
DEFAULT_FROM_NAME="** DEFAULT_FROM_NAME not set **"
