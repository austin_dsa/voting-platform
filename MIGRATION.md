# Migrate from one database to another (eg. postgres to mysql)
This is generic for any django app, but for easy reference I include it here.

The basic process is to dump the database data on the source server and load
the data on the destination server.  We want to do this with django because doing
this at the database level is a pain due to SQL dialect differences.

To dump the data to json, run on the source server:
`python manage.py dumpdata --indent=4 --natural-foreign --traceback > django-dumpdata.json`

Then copy the dump file to the destination server and run:
```
# Clear out the database first
python manage.py sqlflush | python manage.py dbshell
python manage.py loaddata django-dumpdata.json
```
