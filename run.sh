#!/bin/bash
set -x

CDIR=$(dirname "$0")
mkdir -p "$CDIR/logs"
LOG=${1:-"$CDIR/logs/helios.server.`date '+%FT%T%:z'`.log"}
echo "Starting Helios Server `date '+%FT%T%:z'`" >"$LOG"

exec 7>&1
coproc tee -a "$LOG" >&7
exec >&"${COPROC[1]}" 2>&1


. ./env.sh

BIND=${BIND:-${PORT:+"${IP:-0.0.0.0}:${PORT}"}}
exec python manage.py runserver "${BIND}"
